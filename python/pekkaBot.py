import json
import socket
from track import Track
from Car import Car

class PekkaBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.myCar = Car("","")
        self.lastTickTime = 0
        self.tick = 0
        self.startTime = 0
        self.testRound = 1
        self.startDecTime = 0
        self.startDecSpeed = 0
        self.startAccDistance = 0 #Full throttle for this distance from start to get car acceleration
        self.startDecDistance = 0#After startAcc, set throttle to zero for this distance to calculate car decceleration
        #Logging stuff below
        self.speeds = []
        self.accs = []
        self.angles = []
        self.distances = []
        self.pedals = []
        self.frictions = []
        self.timeIntervals = []
    
    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def on_car_positions(self, data):
        #todo: are these needed, same stuff in car.updatefromjson
        index = data[0]['piecePosition']['pieceIndex']
        inPieceDist = data[0]['piecePosition']['inPieceDistance']
        startLane = data[0]['piecePosition']['lane']['startLaneIndex']
        
        current_piece = self.track.piece(index)
        #print("Car positions")
        #print json.dumps(data, sort_keys=True, indent=4, separators=(',', ': '))
        currentTime = self.tick
        if self.lastTickTime != 0:
            timeBetweenTicks = currentTime-self.lastTickTime
        else:
            self.startTime = currentTime
            timeBetweenTicks = 0
        
        self.lastTickTime = currentTime     
        for car in data:
            if car["id"]["name"] == self.myCar.name:
                self.myCar.updateFromJson(car)
                newDistanceFromStart = self.track.distanceFromStart(self.myCar.pieceIndex,self.myCar.inPieceDistance,self.myCar.startlane,self.myCar.lap)
                self.myCar.calculateSpeed(newDistanceFromStart, timeBetweenTicks)
                break

        #Calculate friction. When car starts to slide at an angle, friction should be v*v/r
        pieceRadius = 0
        if self.track.trackPieces[self.myCar.pieceIndex].curve:
            pieceRadius = self.track.trackPieces[self.myCar.pieceIndex].radiusses[self.myCar.startlane]
        
        if pieceRadius == 0:
            self.track.friction = 0
        else:
            self.track.friction = self.myCar.speed*self.myCar.speed/pieceRadius
        
        if self.testRound == 1:
            if self.myCar.distanceFromStart < self.startAccDistance:
                print "testAcc"
                pedal = 1.0
            elif self.myCar.distanceFromStart < (self.startAccDistance + self.startDecDistance):
                print "testDec"
                #When first acceleration ends, calculate car accceleration parameter
                self.myCar.carAcceleration = self.myCar.speed/(currentTime-self.startTime)
                self.startDecTime = currentTime
                self.startDecSpeed = self.myCar.speed
                pedal = 0.0
            else:
                print "testDone"
                #When first acceleration ends, calculate car accceleration parameter
                self.myCar.carDecceleration = (self.myCar.speed-self.startDecSpeed)/(currentTime-self.startDecTime)
                self.testRound = 0
                pedal = 1.0
                print self.myCar.carAcceleration
                print self.myCar.carDecceleration
        else:
            nextcurve, distance = self.track.nextCurve(index, inPieceDist, startLane)
            #check that average friction is calculated, otherwise use default values
            if (self.track.averageFriction > 0):
                targetSpeedForNextCurve = nextcurve.maxSpeedForNoDrift(0.46, self.myCar.startlane)
                if self.track.piece(self.myCar.pieceIndex).isCurve:
                    targetSpeedForThisCurve = self.track.piece(self.myCar.pieceIndex)
                else:
                    targetSpeedForThisCurve = 11 #max speed
                
                #if current speed is less than speedForNextCurve, full throttle
                if self.myCar.speed < targetSpeedForNextCurve:# and self.myCar.speed < targetSpeedForThisCurve:
                    pedal = 1.0
                else:
                    deccelerationDistance = 0.5*self.myCar.carDecceleration*(self.myCar.speed-targetSpeedForNextCurve)*(self.myCar.speed-targetSpeedForNextCurve)
                    if nextcurve.index < index:
                        nextCurveDistanceFromStart = self.track.distanceFromStart(index, 0, self.myCar.startlane, self.myCar.lap-1)
                    else:
                        nextCurveDistanceFromStart = self.track.distanceFromStart(index, 0, self.myCar.startlane, self.myCar.lap)
                    if (self.myCar.distanceFromStart + deccelerationDistance > nextCurveDistanceFromStart):
                        pedal = 0.0
                    else:
                        pedal = 1.0
            else:
                thr = 0.25625 / nextcurve.tightness(startLane)
                if (thr > 1.0): thr = 1.0

                if distance > 250.0:
                    pedal = 1.0
                elif distance > 100:
                    pedal = thr * 0.75
                else:
                    pedal = thr
            
        #print 'pedaling', pedal, 'distance', distance, 'angle', self.myCar.angle
        
        #Store parameters for logging
        self.speeds.append(self.myCar.speed)
        self.accs.append(self.myCar.acceleration)
        self.angles.append(self.myCar.angle)
        self.distances.append(self.myCar.distanceFromStart)
        self.frictions.append(self.track.friction)
        self.pedals.append(pedal)
        self.timeIntervals.append(timeBetweenTicks)

        self.throttle(pedal)

    def on_lap_finished(self,data):
        print("Lap finished")
        if data["car"]["name"] == self.myCar.name:
            if data["lapTime"]["lap"] == 0:
                frictionsSum = 0
                frictionsCount = 0
                for f in self.frictions:
                    if (f != 0):
                        frictionsSum += f
                        frictionsCount += 1
    
                self.track.averageFriction = frictionsSum/frictionsCount
                print self.track.averageFriction
    
        self.ping()
    
    def on_crash(self, data):
        print("Someone crashed")
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.ping()
        
    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()
    
    def on_your_car(self,data):
        print("Your Car")
        self.myCar.name = data["name"]
        self.myCar.color = data["color"]
    
    def on_game_init(self, data):
        print("Game init")
        #print json.dumps(data, sort_keys=True, indent=4, separators=(',', ': '))
        trackObj = data['race']['track']
        self.track = Track(trackObj)
    
        nextCurve, distanceToFirstCurve = self.track.nextCurve(0,0,1)
        self.startAccDistance = distanceToFirstCurve/4
        self.startDecDistance = distanceToFirstCurve/4
    
        #print "Track pieces:"
        #for pc in self.track.trackPieces:
        #    print pc
        
    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'gameInit': self.on_game_init,
            'yourCar': self.on_your_car,
            'lapFinished': self.on_lap_finished
        }
        
        #log_file = open('log.txt', 'w')
        socket_file = self.socket.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            if 'gameTick' in msg:
                self.tick = msg['gameTick']
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()
        #log_file.close()


