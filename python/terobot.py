
import math

from Car import Car
from noobBot import NoobBot


MAX_THROTTLE = 1.0

STATE_WAITING_FOR_START = 0
STATE_TESTING_ACCELERATION = 1
STATE_TESTING_MAX_SPEED = 2
STATE_RACING = 3

class TeroBot(NoobBot):

    def __init__(self, socket, name, key):
        NoobBot.__init__(self, socket, name, key)
        
        self.state = STATE_WAITING_FOR_START
        self.crashed = False
        
        self.lastSwitchCommand = ""
        self.passedSwitch = -1
        
        self.otherJson = {}
        
        self.targetCentrF = 0.4675

    def on_car_positions(self, data):
        
        # ACC = (THR - SPD/MAX) / ACCFAC
        # MAX = -SPD / (ACC * ACCFAC - THR)
        # THR = ACC * ACCFAC + SPD/MAX
        
        myData = None
        for car in data:
            carName = car['id']['name']
            if carName == self.myCar.name:
                myData = car
            else:
                if not carName in self.otherCars:
                    self.otherCars[carName] = Car(carName, car['id']['color'])
                self.otherJson[carName] = car

        if myData == None:
            print 'ERROR: Our data not found'
            self.ping()
            return

        if self.state == STATE_WAITING_FOR_START:
            self.startingPosition = myData['piecePosition']['inPieceDistance']
            self.throttle(MAX_THROTTLE)
        else:
           
            if self.crashed:
                # We can only wait..
                self.ping()
                return
            
            myPosition = myData['piecePosition']
            index = myPosition['pieceIndex']
            inPieceDist = myPosition['inPieceDistance']
            startLane = myPosition['lane']['startLaneIndex']
            
            if self.state == STATE_TESTING_ACCELERATION:
                if inPieceDist > self.startingPosition:
                    # Just started moving, grab the acceleration factor from the inverse of covered distance.
                    self.accelerationFactor = 1.0 / (inPieceDist - self.startingPosition)
                    self.firstMovementTick = self.tick
                    self.state = STATE_TESTING_MAX_SPEED
                else:
                    # Haven't started moving yet, wait and press the pedal to the floor.
                    print 'waiting for movement..'
                    self.throttle(MAX_THROTTLE)
                    self.lastTick = self.tick
                    return

            current_piece = self.track.piece(index)

            oldAngle = self.myCar.angle
            self.myCar.updateFromJson(myData)

            tickDelta = self.tick - self.lastTick
            self.lastTick = self.tick

            self.myCar.calculateSpeed(tickDelta, self.track, self.myCar.startlane != self.myCar.endlane)
            self.myCar.calculateAngleSpeed(oldAngle, tickDelta)

            for other in self.otherCars:
                otherCar = self.otherCars[other]
                otherCar.updateFromJson(self.otherJson[otherCar.name])

            if self.state == STATE_TESTING_MAX_SPEED and self.tick > self.firstMovementTick + 1:
                # Moved enough to calculate maximum speed.
                self.maxSpeed = (self.myCar.acceleration - self.myCar.speed) / (self.myCar.acceleration * self.accelerationFactor - MAX_THROTTLE)
                self.myCar.carAcceleration = self.myCar.speed / 3
                self.state = STATE_RACING

            nextcurve, distance = self.track.nextCurve(index, inPieceDist, startLane, 0.1)

            pedal = MAX_THROTTLE

            if self.state == STATE_RACING:

                nextSwitch, switchDistance = self.track.nextSwitch(index, inPieceDist, startLane)
                if switchDistance < 35.0 and self.passedSwitch != nextSwitch.index:
                    command = self.track.commandForNextSwitch(self.myCar, self.otherCars)
                    if command != "":
                        self.switch(command)
                        self.passedSwitch = nextSwitch.index
                        return
                    else:
                        self.passedSwitch = nextSwitch.index
                
                if current_piece.index == self.track.indexOfLongestStraight and self.myCar.turbo == True:
                    self.turbo("TURBO")
                    return;
                
                # Smallest radius in the next group of curve pieces.
                pieceRadius = nextcurve.smallestRadius(self.myCar.startlane)
                
                # Target speed in a curve.
                targetSpeed = math.sqrt(self.targetCentrF * pieceRadius)
 
                if targetSpeed > self.maxSpeed:
                    targetSpeed = self.maxSpeed
 
                k = 1.0 / (self.accelerationFactor * self.maxSpeed)
                decelerationDistance = (targetSpeed - self.myCar.speed) / -k
                
                if 0 < distance < decelerationDistance:
                    targetPedal = 0.0
                    pedal = targetPedal
                    
                elif distance == 0:

                    toTarget = targetSpeed - self.myCar.speed
                    targetAcceleration = toTarget / 10.0
                    targetPedal = targetAcceleration * self.accelerationFactor + self.myCar.speed / self.maxSpeed
                
                    if targetPedal > MAX_THROTTLE:
                        targetPedal = MAX_THROTTLE
                    elif targetPedal < 0.0:
                        targetPedal = 0.0
                        
                    pedal = targetPedal

                else:
                    pedal = MAX_THROTTLE
                    
            if self.state == STATE_RACING:
                predictionDelta = 10 # ticks
                anglePrediction = self.myCar.angle + predictionDelta * self.myCar.angleSpeed + 0.5 * predictionDelta * predictionDelta * self.myCar.angleAcceleration
                if abs(anglePrediction) > 50.0:
                    if self.targetCentrF > 0.45:
                        self.targetCentrF -=  0.0001
                    print 'brakes on, new targetCentrF:', self.targetCentrF
                    pedal = 0
            
            self.throttle(pedal)

    def on_game_start(self, data):
        if self.state == STATE_WAITING_FOR_START:
            self.state = STATE_TESTING_ACCELERATION
        NoobBot.on_game_start(self, data)
        
    def on_game_end(self, data):
        self.lastSwitchCommand = ""
        self.passedSwitch = -1
        NoobBot.on_game_end(self, data)
        
    def on_crash(self, data):
        if data['name'] == self.myCar.name:
            print 'CRAASH'
            self.crashed = True

    def on_spawn(self, data):
        if data['name'] == self.myCar.name:
            self.crashed = False
