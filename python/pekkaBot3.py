import json
import socket
import math
from track import Track
from Car import Car
from noobBot import NoobBot

STATE_WAITING_FOR_START = 0
STATE_TESTING_ACCELERATION = 1
STATE_TESTING_FRICTION = 2
STATE_RACING = 3
START_TEST_SPEED = 6.25
RACE_MAX_CURVE_SPEED = 9.75


class PekkaBot3(NoobBot):

    def __init__(self, socket, name, key):
        NoobBot.__init__(self, socket, name, key)
        self.state = STATE_WAITING_FOR_START
        self.currentCurvePieceIndex = -1
        self.curveGroupDatas = []
        self.curveGroupSpeeds = {}
        self.maxAngleForCurrentCurveGroup = 0
        self.lastSwitchCommand = ""
        self.xFactor = 0
        
    def on_car_positions(self, data):
        # print json.dumps(data, sort_keys=True, indent=4, separators=(',', ': '))
        #update data for myCar
        tickDelta = self.tick - self.lastTick
        self.lastTick = self.tick
        
        for car in data:
            if car["id"]["name"] == self.myCar.name:
                oldAngle = self.myCar.angle
                self.myCar.updateFromJson(car)
                newDistanceFromStart = self.track.distanceFromStart(self.myCar.pieceIndex, self.myCar.inPieceDistance, self.myCar.startlane, self.myCar.lap)
                self.myCar.calculateSpeed(newDistanceFromStart, tickDelta)
                break
        
        self.currentPiece = self.track.trackPieces[self.myCar.pieceIndex]
        pedal = START_TEST_SPEED/10
        
        #start testing acceleration
        if self.state == STATE_WAITING_FOR_START:
            print "testing acc"
            self.state = STATE_TESTING_ACCELERATION
            pedal = 1.0
        elif self.state == STATE_TESTING_ACCELERATION:
            pedal = 1.0
            #when test speed is achieved, calculate car acceleration and start testing friction
            if self.myCar.speed >= START_TEST_SPEED:
                self.myCar.carAcceleration = self.myCar.speed/self.tick
                self.state = STATE_TESTING_FRICTION
                print "achieved test speed, acceleration",self.myCar.carAcceleration
        elif self.state == STATE_TESTING_FRICTION: 
            command = self.track.commandForNextSwitch(self.myCar)
            if command != self.lastSwitchCommand:
                self.lastSwitchCommand = command
                print "send switch"
                self.switch(command)
                return
            
            testingEnded = False     
            #check for piece changes during first lap
            if self.currentCurvePieceIndex == self.currentPiece.curveGroupIndex and self.currentPiece.curveGroupIndex != -1:
                if abs(self.myCar.angle) > self.maxAngleForCurrentCurveGroup:
                    self.maxAngleForCurrentCurveGroup = abs(self.myCar.angle)
            else:
                if self.currentCurvePieceIndex != -1:
                    self.curveGroupDatas.append(self.maxAngleForCurrentCurveGroup)
                    if len(self.curveGroupDatas) > self.track.curveGroupCount:
                        testingEnded = True
                self.currentCurvePieceIndex = self.currentPiece.curveGroupIndex
                self.maxAngleForCurrentCurveGroup = 0

            
            if testingEnded == True:
                print self.curveGroupDatas
                
                #find maximum angle from curveGroups
                maxIndex = 0
                maxAngle = 0
                
                for i in range(0,len(self.curveGroupDatas)):
                    curveAngle = self.curveGroupDatas[i]
                    if curveAngle > maxAngle:
                        maxIndex = i
                        maxAngle = curveAngle
                        
                #compute xFactor = (RACE_MAX_CURVE_SPEED-START_TEST_SPEED)/sqrt(angleForMaxGroup)
                #maxAngle = 1.1*maxAngle
                    
                self.xFactor = (RACE_MAX_CURVE_SPEED-START_TEST_SPEED)/math.sqrt(maxAngle)
                #compute speeds for curveGroups
                #targetSpeed = RACE_MAX_CURVE_SPEED-xFactor*sqrt(angleForGroup)
                for i in range(0,len(self.curveGroupDatas)):
                    angle = self.curveGroupDatas[i]
                    self.curveGroupSpeeds[i] = RACE_MAX_CURVE_SPEED-self.xFactor*math.sqrt(angle)
                
                
                print "speeds",self.curveGroupSpeeds
          
                self.state = STATE_RACING
                print "racing"
        elif self.state == STATE_RACING:
            if self.currentPiece.index == self.track.indexOfLongestStraight and self.myCar.turbo == True:
                print "TURBO LAUNCHED"
                self.turbo("TURBO")
                return;
            
            command = self.track.commandForNextSwitch(self.myCar)
            if command != self.lastSwitchCommand:
                self.lastSwitchCommand = command
                print "send switch"
                self.switch(command)
                return
            
            nextcurve, distance = self.track.nextCurve(self.myCar.pieceIndex, self.myCar.inPieceDistance, self.myCar.endlane)
            targetSpeedForNextCurve = self.curveGroupSpeeds[nextcurve.curveGroupIndex]
            
            if self.currentPiece.curve == False:
                targetSpeedForThisCurve = 999
            else:
                targetSpeedForThisCurve = self.curveGroupSpeeds[self.currentPiece.curveGroupIndex]
            
            
            vi = self.myCar.speed
            dv = self.myCar.speed-targetSpeedForNextCurve
            a = self.myCar.carAcceleration
            deccelerationDistance = vi*dv/a - 0.5*dv*dv/a
            
            if distance < abs(deccelerationDistance):
                if self.myCar.speed > targetSpeedForNextCurve:
                    pedal = 0
                else:
                    pedal = targetSpeedForNextCurve/10
            else:
                #pedal = 1.0
                pedal = targetSpeedForThisCurve/10
                if pedal > 1.0:
                    pedal = 1.0
            
            #print "targetSpeedForThisCurve",targetSpeedForThisCurve,"targetSpeedForNextCurve",targetSpeedForNextCurve,"speed",self.myCar.speed,"pedal",pedal
        else:
            print"Unknown state"
        
        #Store parameters for logging
#        self.loggingArray.append((self.myCar.angle,self.myCar.speed,currentPiece.radiusses[self.myCar.startlane],currentPiece.angle))
        self.throttle(pedal)

    def on_crash(self, data):
        print("Someone crashed")
        index = self.currentPiece.curveGroupIndex
        print index
        if index != -1:
            speed = self.curveGroupSpeeds[index]
            self.curveGroupSpeeds[index] = 0.9*speed
        
        print self.curveGroupSpeeds
        self.ping()
            
