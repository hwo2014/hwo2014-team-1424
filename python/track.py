
import math
from Car import Car

class Track(object):
    
    def __init__(self, trackObj):
        
        self.trackPieces = []
        self.laneLengths = []
        self.friction = 0
        self.averageFriction = 0
        
        # get lane offsets from center of the road        
        laneOffsets = []
        for lane in trackObj['lanes']:
            laneOffsets.append(lane['distanceFromCenter'])

        # create track pieces
        pieceIndex = 0
        for piece in trackObj['pieces']:
            trackPc = TrackPiece()
            if (pieceIndex > 0):
                self.trackPieces[pieceIndex - 1].setNext(trackPc)
                trackPc.setPrevious(self.trackPieces[pieceIndex-1])

            if 'length' in piece:
                trackPc.straight(piece['length'], laneOffsets, 'switch' in piece and piece['switch'] == True)
                trackPc.index = pieceIndex
                self.trackPieces.append(trackPc)
            else:
                trackPc.curve(piece['angle'], piece['radius'], laneOffsets, 'switch' in piece and piece['switch'] == True)
                trackPc.index = pieceIndex
                self.trackPieces.append(trackPc)
            pieceIndex += 1
            
        # after the last piece the track continues back on the first piece
        self.trackPieces[pieceIndex - 1].setNext(self.trackPieces[0])
        self.trackPieces[0].setPrevious(self.trackPieces[pieceIndex-1])
        
        # calculate lane lengths
        for laneIndex in range(len(trackObj['lanes'])):
            laneLen = 0.0
            for piece in self.trackPieces:
                laneLen += piece.length(laneIndex)
            self.laneLengths.append(laneLen)
            
        self.indexOfLongestStraight = self.findIndexOfLongestStraight()

        #Find continuous curves with same angle and radius and group them together
        curveGroupIndex =-1
        currentAngle = 0
        currentRadius = 0
        for piece in self.trackPieces:
            if piece.curve == True:
                if piece.angle == currentAngle and piece.radiusses[0] == currentRadius:
                    piece.curveGroupIndex = curveGroupIndex
                else:
                    curveGroupIndex += 1
                    currentAngle = piece.angle
                    currentRadius = piece.radiusses[0]
                    piece.curveGroupIndex = curveGroupIndex
            else:
                currentAngle = 0
        
        self.curveGroupCount = curveGroupIndex
    
    def piece(self, index):
        return self.trackPieces[index]
    
    def previousCurve(self,index):
        startPiece = self.trackPieces[index]
        if (startPiece.isCurve()):
            return (startPiece, 0)
        
        distance = 0.0
        
        while (not startPiece.isCurve()):
            distance += startPiece.length(0)
            startPiece = startPiece.previous

        return (startPiece, distance)
    
    def nextCurve(self, index, pieceDistance, lane, threshold=0.0):

        startPiece = self.trackPieces[index]
        if (startPiece.isCurve()):
            return (startPiece, 0)
        
        distance = -pieceDistance
        while (not startPiece.isCurve()) or (startPiece.tightness(lane) <= threshold):
            distance += startPiece.length(lane)
            startPiece = startPiece.next

        return (startPiece, distance)
    
    
    def nextSwitch(self, index, pieceDistance, lane):

        startPiece = self.trackPieces[index]
        if startPiece.hasSwitch():
            return (startPiece, 0)

        distance = -pieceDistance
        while not startPiece.hasSwitch():
            distance += startPiece.length(lane)
            startPiece = startPiece.next

        return (startPiece, distance)
        
    def commandForNextSwitch(self, car, others=None):
        carNextSwitch, _ = self.nextSwitch(car.pieceIndex, car.inPieceDistance, car.endlane)
        shortestLaneIndex = -1
        shortestLen = 999999999999
        laneIndex = 0

        blockedLanes = []
        if others:
            pieceIndex = car.pieceIndex
            # Check until we find the switch after the next switch.
            while not (pieceIndex != carNextSwitch.index and self.piece(pieceIndex).hasSwitch()):
                for otherKey in others:
                    other = others[otherKey]
                    if other.pieceIndex == pieceIndex:
                        if pieceIndex > car.pieceIndex or other.inPieceDistance > car.inPieceDistance:
                            blockedLanes.append(other.endlane)
                pieceIndex += 1
                if pieceIndex == len(self.trackPieces):
                    pieceIndex = 0 # Wrap around the track.
                
        for _ in self.laneLengths:
            _, laneLen = self.nextSwitch(carNextSwitch.index + 1, 0, laneIndex)

            if laneIndex in blockedLanes:
                laneLen *= 2.0
            
            if laneLen < shortestLen:
                shortestLen = laneLen
                shortestLaneIndex = laneIndex
            laneIndex += 1

        if shortestLaneIndex < car.endlane:
            return "Left"
        elif shortestLaneIndex > car.endlane:
            return "Right"
        else:
            return ""
    
    # Palauttaa radan kokonaispituuden yhta lanea pitkin
    def lapTotalLength(self, lane):
        return self.laneLengths[lane]
    
    def findIndexOfLongestStraight(self):
        longestStraightLength = 0
        longestStraightStartIndex = 0
        thisStraightLength = 0
        thisStraightStartIndex = -1
        startStraightLength = 0
        
        for piece in self.trackPieces:
            if piece.curve != True:
                if thisStraightStartIndex == -1:
                    thisStraightStartIndex = piece.index
                thisStraightLength += piece.lengths[0]
            else:
                if thisStraightLength > longestStraightLength:
                    longestStraightLength = thisStraightLength
                    longestStraightStartIndex = thisStraightStartIndex
                    
                if thisStraightStartIndex == 0:
                    startStraightLength = thisStraightLength
                    
                thisStraightLength = 0
                thisStraightStartIndex = -1
        
        #Add start straight length to last straght on lap
        if thisStraightLength > 0:
            thisStraightLength += startStraightLength
            if thisStraightLength > longestStraightLength:
                longestStraightLength = thisStraightLength
                longestStraightStartIndex = thisStraightStartIndex
        
        return longestStraightStartIndex       

class TrackPiece(object):

    def straight(self, length, laneOffsets, switch):
        self.lengths = []
        self.tightnesses = []
        self.radiusses = []
        for _ in laneOffsets:
            self.lengths.append(length)
            self.tightnesses.append(0)
            self.radiusses.append(0)
        self.curve = False
        self.switch = switch
        self.angle = 0
        self.curveGroupIndex = -1


    def curve(self, angle, radius, laneOffsets, switch):
        self.lengths = []
        self.tightnesses = []
        self.radiusses = []
        self.radius = radius
        self.angle = angle
        self.curveGroupIndex = 0
        if angle > 0:
            for offset in laneOffsets:
                self.lengths.append(2 * math.pi * (radius - offset) / 360.0 * angle)
                self.tightnesses.append(angle / (radius - offset))
                self.radiusses.append(radius - offset)
        else:
            for offset in laneOffsets:
                self.lengths.append(2 * math.pi * (radius + offset) / 360.0 * abs(angle))
                self.tightnesses.append(abs(angle) / (radius + offset))
                self.radiusses.append(radius + offset)
        
        self.curve = True
        self.switch = switch
        
    def setNext(self, nextPiece):
        self.next = nextPiece
    
    def setPrevious(self, previousPiece):
        self.previous = previousPiece
        
    def isStraight(self):
        return not self.curve
        
    def isCurve(self):
        return self.curve
    
    def length(self, laneIndex):
        return self.lengths[laneIndex]
    
    def tightness(self, laneIndex):
        return self.tightnesses[laneIndex]
    
    def hasSwitch(self):
        return self.switch
    
    def smallestRadius(self, laneIndex):
        piece = self
        smallest = 99999999999999
        piecesToCheck = 3
        while piece.isCurve() and piecesToCheck > 0:
            if piece.radiusses[laneIndex] < smallest:
                smallest = piece.radiusses[laneIndex]
            piece = piece.next
            piecesToCheck -= 1
        return smallest
    
    def centripetalDistance(self, laneIndex):
        centriF = 0.0
        piece = self
        while piece.isCurve():
            centriF += piece.length(laneIndex) / piece.radiusses[laneIndex]
            piece = piece.next
        return centriF
    
    def maxSpeedForNoDrift(self,friction,lane):
        return math.sqrt(self.radiusses[lane]*friction)
    
    def __str__(self):
        if self.curve:
            return "curve {0} units,  tightness {1}{2}".format(self.lengths, self.tightnesses, ", has switch" if self.switch else "")
        else:
            return "straight {0} units{1}".format(self.lengths[0], ", has switch" if self.switch else "")
