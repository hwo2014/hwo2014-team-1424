import csv
import matplotlib.pyplot as plt
import numpy as np
import math

ticks = []
speeds = []
carAngles = []
carAngleSpeeds = []
carAngleAccs = []
radiusses = []
trackAngles = []


UNKNOWN = -1
UNDER = 1
CRITICAL = 2
OVER = 3

def predictAngle(angle,angleSpeed,k,c,t):
    A1=angle#angle at start
    wtf = math.sqrt(4*k-c*c)/2
    B1=(angleSpeed+(c/2)*angle)/wtf
    fiih = math.atan(A1/B1)
    A=math.sqrt(A1*A1+B1*B1)
                    
    predictedAngleFromOscillator =angle/abs(angle)*A*math.exp(-c*t/2)*math.sin(wtf*t+fiih)
    
    return predictedAngleFromOscillator

def predictDrivingAngle(speed,radius,omega,fiid,c,k,t):
    #v2r-c ?
    A = (speed*speed/radius)/math.sqrt((k-omega*omega)**2+4*c*c*omega**2)
    fii = math.atan(c*omega/(k-omega**2))-fiid
    
    predictedAngle = A*math.cos(omega*t-fii)
    
    return predictedAngle

def predictTotalAngle(angle,angleSpeed,k,c,t,carSpeed,radius,omega,fiid,ug):
    gamma = c/2
    omega0 = math.sqrt(k)
    
    if radius == 0:
        F0 = 0
    else:
        F0 = (carSpeed**2)/radius
    
    if F0 < ug:
        F0 = 0
        
    A = F0/math.sqrt((omega0**2-omega**2)**2+4*gamma**2*omega**2)
    fii = math.atan(c*omega/(k-omega**2))-fiid
    omegaPilkku = math.sqrt(omega0**2-gamma**2)
    fiihOsoittaja = omegaPilkku*(angle-A*math.cos(fii))
    fiihNimittaja = angleSpeed + gamma*(angle-A*math.cos(fii))-A*omega*math.sin(fii)
    fiih = math.atan(fiihOsoittaja/fiihNimittaja)
    Ah = (angle-A*math.cos(fii)/math.sin(fiih))
    
    predTransient = Ah*math.exp(-gamma*t)*math.sin(omegaPilkku*t+fiih)
    
    predTransient = predictAngle(angle,angleSpeed,k,c,t)
    predSteady = A*math.cos(omega*t-fii)
    
    #print "predTransient",predTransient,"predSteady",predSteady
    
    predAngle = predTransient + predSteady
    
    return predAngle
    

with open('log.csv','rb') as f:
    reader = csv.reader(f)
    for row in reader:
        ticks.append(int(row[0]))
        speeds.append(float(row[1]))
        carAngles.append(float(row[2]))
        carAngleSpeeds.append(float(row[3]))
        carAngleAccs.append(float(row[4]))
        radiusses.append(float(row[5]))
        trackAngles.append(float(row[6]))
        
A1=0
damping = UNKNOWN
k = 0.0086688
c = 0.10238
s = c/2
w0 = math.sqrt(k)
wpilkku = math.sqrt(w0*w0-s*s)
ug = 0.42
predictedAngles = []
predictedMaxAngles = []
realAngles = []
kVals = []
cVals = []
xVals = []
ugs = []
angleData = []
v2rData = []
predictionStartTick = -1
ticksIntoStraight = 0
ticksIntoAngle = 0


for i in range(2,len(speeds)-1):
    #Shortcut variables
    tick = ticks[i]
    prevTick = ticks[i-1]
    
    if tick-prevTick != 1:
        print "TICKS WRONG tick",tick,"prevTick",prevTick
    
    trackAngle = trackAngles[i]
    angle = carAngles[i]
    prevAngle = carAngles[i-1]
    prevPrevAngle = carAngles[i-2]
    speed = speeds[i]
    radius = radiusses[i]
    prevRadius = radiusses[i-1]
    prevPrevRadius = radiusses[i-2]
    angleSpeed = carAngleSpeeds[i]
    prevAngleSpeed = carAngleSpeeds[i-1]
    prevPrevAngleSpeed = carAngleSpeeds[i-2]
    angleAcc = carAngleAccs[i]
    prevAngleAcc = carAngleAccs[i-1]  
    prevPrevAngleAcc = carAngleAccs[i-2]
    
    if radius != 0:
        ticksIntoAngle = ticksIntoAngle+1  
        ticksIntoStraight = 0   
        v2r = (speeds[i]*speeds[i])/radius
    else:
        ticksIntoAngle = 0
        ticksIntoStraight = ticksIntoStraight + 1  
        v2r = 0
            
    if prevRadius != 0:
        v2rPrev = (speeds[i-1]*speeds[i-1])/prevRadius
    else:
        v2rPrev = 0
        
    if prevPrevRadius != 0:
        v2rPrevPrev = (speeds[i-2]*speeds[i-2])/prevPrevRadius
    else:
        v2rPrevPrev = 0        
            
    #Find out ug from speed and radius of first slip
    if radius != 0:
        if prevAngle == 0 and angle != 0 and ug == 0:
            ug = v2r
            ugs.append(ug)    
    
    #Predict max angles
    if ticksIntoAngle == 1:
        ticksUntilNextStraight = 0
        for nextTry in range(tick,len(radiusses)):
            if radiusses[nextTry] != 0:
                ticksUntilNextStraight = ticksUntilNextStraight +1
            else:
                break
        #if v2r > 0.95*ug:
        #    maxAngle = trackAngle/abs(trackAngle)*(v2r-c)/k
        #else:
        #    maxAngle = 0
        #maxInputAngle = k*angle+c*angleSpeed
        #totalAngle = maxAngle+maxInputAngle
        #predictDrivingAngle(speed,radius,omega,fiid,c,k,t):
        #predictTotalAngle(angle,angleSpeed,k,c,t,carSpeed,radius,omega,fiid,ug):

        if trackAngle < 0:
            fiid = math.pi
        else:
            fiid = 0
        
        if angleSpeed != 0:
            predAngle = predictTotalAngle(angle,angleSpeed,k,c,ticksUntilNextStraight,speed,radius,0,fiid,ug)
        else:
            predAngle = 0
        #predDrAngle = predictDrivingAngle(speed,radius,0,math.pi,c,k,ticksUntilNextStraight)
        #predictedMaxAngles.append(predDrAngle)
        print "tick",tick,"angle enterAngle",angle,"predCentrAngle",predAngle
    if ticksIntoStraight == 1:
        ticksUntilNextAngle = 0
        for nextTry in range(tick,len(radiusses)):
            if radiusses[nextTry] == 0:
                ticksUntilNextAngle = ticksUntilNextAngle +1
            else:
                break

        #if angle != 0 and angleSpeed != 0:
        #    predOscAngleAtEndOfStraight = predictAngle(angle,angleSpeed,k,c,ticksUntilNextAngle)
        #else:
        #    predOscAngleAtEndOfStraight = 0
        if angleSpeed != 0:
            predAngle = predictTotalAngle(angle,angleSpeed,k,c,ticksUntilNextAngle,speed,0,0,0,ug)
        else:
            predAngle = 0
        
        print "tick",tick,"straight enterAngle",angle,"predOscAngleAtEndOfStraight",predAngle


        
"""
    #Calculate     
    if angle != 0 and prevAngle != 0 and prevPrevAngle != 0 and radius != 0 and prevRadius != 0 and prevPrevRadius != 0:
        if True:#damping == UNKNOWN:
            #xval=np.array([[angle,angleSpeed],[prevAngle,prevCarAngleSpeed]])
            #yval=np.array([angleAcc-v2r,prevCarAngleAcc-v2r])
            #xval=np.array([[angleAcc,angleSpeed,angle],[prevAngleAcc,prevAngleSpeed,prevAngle],[prevPrevAngleAcc,prevPrevAngleSpeed,prevPrevAngle]])
            #yval=np.array([0,0,0]) 
            xval=np.array([[angle,angleSpeed,v2r*ticksIntoAngle],[prevAngle,prevAngleSpeed,v2rPrev*(ticksIntoAngle-1)],[prevPrevAngle,prevPrevAngleSpeed,v2rPrevPrev*(ticksIntoAngle-2)]])
            yval=np.array([-angleAcc,-prevAngleAcc,-prevPrevAngleAcc])               
            x = np.linalg.solve(xval,yval)
            #if radius !=  0 and tick < 306:
                #print "tick",tick,"v2r",v2r,"ug",ug,"val",angleAcc+k*angle+c*angleSpeed
                #angleData.append(angleAcc+k*angle+c*angleSpeed)
                
            k = x[0]
            c = x[1]
            x = x[2]
            kVals.append(k)
            cVals.append(c)
            xVals.append(x)
            s = c/2
            #if abs(k) > 1:#5*np.median(kVals):
            #    print "tick",ticks[i],"k",k,"angle",angle,"v",speeds[i],"radius",radius,"angleSpeed",angleSpeed,"angleAcc",angleAcc
            #    print "prevTick",ticks[i-1],"k",k,"prevAngle",prevAngle,"prevV",speeds[i-1],"prevRadius",prevRadius,"prevAngleSpeed",prevAngleSpeed,"prevAngleAcc",prevAngleAcc

            #w = w0 = math.sqrt(k)
            #w1 = math.sqrt(w0*w0-s*s)
            print "tick",tick,"k",k,"c",c,"x",x,"r",radius
                        
            if c*c > 4*k:
                damping = OVER
                #print "overdamped"
            elif c*c == 4*k:
                damping = CRITICAL
                #print "criticallydamped"
            else:
                damping = UNDER
                #print "underdamped"
            #print "found k",k,"c",c
            
        else:
            if damping != UNKNOWN and len(predictedAngles) == 0:
                print "predicting angles"
                #x=e^(-s*t)*a*cos(w1*t-alpha)
                #predictedAngle = math.exp(-s*t)*a*math.cos(w1*t-alpha)
                A1=angle#angle at start
                wtf = math.sqrt(4*k-c*c)/2
                B1=(angleSpeed+(c/2)*angle)/wtf
                fii = math.atan(A1/B1)
                A=math.sqrt(A1*A1+B1*B1)
                predictionStartTick = tick
                print "pred A",A,"c",c,"wtf",wtf,"fii",fii
                for t in range(0,20):
                    y = A*math.exp(-c*t/2)*math.sin(wtf*t+fii+3.14)#+3.14 gives correct sign
                    if radiusses[predictionStartTick+t] == 0:
                        predictedAngles.append(y)
                        realAngles.append(carAngles[predictionStartTick+t])
            

plt.figure(1)
plt.plot(kVals,'r')
plt.plot(cVals,'b')
plt.plot(xVals,'g')
plt.show()

print "kVals median",np.median(kVals),"std",np.std(kVals)
print "cVals median",np.median(cVals),"std",np.std(cVals)
print "xVals median",np.median(xVals),"std",np.std(xVals)

#print predictedAngles
#print realAngles

#PredictAngles

predictionStartTick = 0
predictedAngles = []
print "k",k,"ug",ug
for t in range(predictionStartTick,len(ticks)):
    speed = speeds[t]
    trackAngle = trackAngles[t]
    if t == 0:
        angle = 0
    else:
        angle = predictedAngles[t-1]
    
    if t <= 1:
        angleSpeed = 0
    else:
        angleSpeed = angle-predictedAngles[t-2]
    
    radius = radiusses[t]
    if radius != 0:
        v2r = speed*speed/radius
        angAcc = speed/radius
        ticksIntoAngle = ticksIntoAngle + 1
        ticksIntoStraight = 0
        if ticksIntoAngle == 1:
            if v2r > 0.9*ug:
                #print "cocl"
                maxAngle = (v2r-c)/k
            else:
                maxAngle = angle
                
            #print "tick",t,"maxAngle",maxAngle,"v",speed,"r",radius,"v2r",v2r
    else:
        v2r = 0
        angAcc = 0
        ticksIntoAngle = 0
        ticksIntoStraight = ticksIntoStraight + 1        
    
    
    #Predict angle from oscillator
    if angle != 0 and angleSpeed != 0:
        A1=angle#angle at start
        wtf = math.sqrt(4*k-c*c)/2
        B1=(angleSpeed+(c/2)*angle)/wtf
        fii = math.atan(A1/B1)
        A=math.sqrt(A1*A1+B1*B1)
                    
        predictedAngleFromOscillator = -angle/abs(angle)*A*math.exp(-c*t/2)*math.sin(wtf*t+fii)
    else:
        predictedAngleFromOscillator = 0
        
    if radius != 0 and v2r > ug:
        predictedAngleFromCentrifugal = trackAngle/abs(trackAngle)*(v2r-ug)*ticksIntoAngle*ticksIntoAngle
    else:
        predictedAngleFromCentrifugal = 0
    
    
    totalAngle = predictedAngleFromOscillator + predictedAngleFromCentrifugal
    
    #if t > 250 and t<420:
        #print "tick",t,"predictedAngleFromOscillator",predictedAngleFromOscillator,"predictedAngleFromCentrifugal",predictedAngleFromCentrifugal
    #predictedAngles.append(totalAngle)
    maxCentrAngle = (v2r-c)/k
    #maxOscillAngle = angle-maxCentrAngle*math.cos(fii)/math.sin(fiih)
    if radius != 0 and v2r > ug:
        predictedCentrAngle = trackAngle/abs(trackAngle)*((v2r-c)/k)*math.sin(0.5*math.sqrt(k)*ticksIntoAngle)
    else:
        predictedCentrAngle = 0
    
    if t >= 1:
        if radiusses[t-1] != 0 and radiusses[t] == 0:
            A1 = 0
        elif radiusses[t] == 0 and radiusses[t-1] != 0:
            A1 = 0
            
        angle = predictedAngles[t-1]
        angleSpeed = predictedAngles[t-2]-angle
    else:
        angle = 0
        angleSpeed = 0
            
    if angle != 0 and angleSpeed != 0:
        if A1 == 0:
            A1=angle#angle at start
            wtf = math.sqrt(4*k-c*c)/2
            B1=(angleSpeed+(c/2)*angle)/wtf
            fii = math.atan(A1/B1)
            A=math.sqrt(A1*A1+B1*B1)
            currentTicks = 1
            print "Recalculated oscillation params A",A,"c",c,"wtf",wtf,"fii",fii

        #if radius != 0:
        #    currentTicks = ticksIntoAngle
        #else:
        #    currentTicks = ticksIntoStraight
        
        predictedAngleFromOscillator = angle/(abs(angle))*A*math.exp(-c*currentTicks/2)*math.sin(wtf*currentTicks+fii)
        currentTicks = currentTicks + 1 
    else:
        A1 = 0
        predictedAngleFromOscillator = 0#angle
    
    if t >= 305:
        predictedCentrAngle = 0
    #if predictedAngleFromOscillator != 0:
    #    predictedCentrAngle = 0
    
    if t > 250 and t < 500:
        print "tick",t,"predictedCentrAngle",predictedCentrAngle,"predictedAngleFromOscillator",predictedAngleFromOscillator,"realAngle",carAngles[t]
    predictedAngles.append(predictedCentrAngle + predictedAngleFromOscillator)
        
#pred A 37.826366432 c 0.102382146765 wtf 0.0777711621647 fii 1.22473306556

#print predictedAngles
        
            

#print "realAngles",realAngles
#print "predictedAngles",predictedAngles

#print "kVals median",np.median(kVals),"std",np.std(kVals)
#print "cVals median",np.median(cVals),"std",np.std(cVals)
#print "ugs", ugs


plt.figure(1)
plt.plot(predictedAngles,'r')
plt.plot(carAngles,'g')
plt.show()


plt.subplot(211)
plt.plot(kVals)

plt.subplot(212)
plt.plot(cVals)

plt.subplot(411)
plt.plot(speeds)
plt.ylabel('speeds')

plt.subplot(412)
plt.plot(carAngles)
plt.ylabel('carAngles')

plt.subplot(413)
plt.plot(trackAngles)
plt.ylabel('trackAngles')

plt.subplot(414)
plt.plot(radiusses)
plt.ylabel('radiusses')

plt.subplot(313)
plt.plot(carAngles)
plt.ylabel('carAngles')

plt.subplot(414)
plt.plot(accs)
plt.ylabel('accelerations')

plt.subplot(716)
plt.plot(angles)
plt.ylabel('angles')

plt.subplot(717)
plt.plot(frictions)
plt.ylabel('frictions')

plt.show()
"""





