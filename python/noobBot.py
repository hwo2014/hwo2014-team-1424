import json
from track import Track
from Car import Car

class NoobBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.myCar = Car("", "")
        self.tick = 0
        self.lastTick = -1
        self.qualifyingDone = False # Qualifying rounds first.
        self.quickRace = False
        
        self.otherCars = {}
        
        #Logging stuff below
        self.loggingArray = []
    
    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def join(self, trackName, trackPassword, trackCarCount):
        if trackName == "":
            print "joining anonymous race"
            self.msg("join", {"name": self.name, "key": self.key})
        else:
            print "joining race " + trackName 
            self.msg("joinRace", {"botId":{"name" : self.name, "key" : self.key},"trackName" : trackName, "password" : trackPassword,"carCount" : int(trackCarCount) })

    def create_race(self, trackName, trackPassword, trackCarCount):
        print "create_race"
        self.msg("createRace", {"botId": { "name" : self.name, "key" : self.key}, "trackName" : trackName, "password" : trackPassword,"carCount" : int(trackCarCount) })
        
    def throttle(self, throttle):
        self.msg("throttle", throttle)
    
    def switch(self, switch):
        self.msg("switchLane", switch)

    def turbo(self, message):
        self.msg("turbo", message)
        self.myCar.turbo = False
        
    def ping(self):
        self.msg("ping", {})

    def run(self, create, trackName, trackPassword, trackCarCount):
        if create == "create":
            self.create_race(trackName, trackPassword, trackCarCount)
        else:
            self.join(trackName, trackPassword, trackCarCount)
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()
        
    def on_turbo(self,data):
        print("Got turbo")
        self.myCar.turbo = True
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def on_car_positions(self, data):
        # print json.dumps(data, sort_keys=True, indent=4, separators=(',', ': '))
        tickDelta = self.tick - self.lastTick
        self.lastTick = self.tick
        for car in data:
            if car["id"]["name"] == self.myCar.name:
                oldAngle = self.myCar.angle
                self.myCar.updateFromJson(car)
                newDistanceFromStart = self.track.distanceFromStart(self.myCar.pieceIndex, self.myCar.inPieceDistance, self.myCar.startlane, self.myCar.lap)
                self.myCar.calculateSpeed(newDistanceFromStart, tickDelta)
                break

        current_piece = self.track.piece(self.myCar.pieceIndex)

        #Calculate friction. When car starts to slide at an angle, friction should be v*v/r
        pieceRadius = 0
        if self.track.trackPieces[self.myCar.pieceIndex].curve:
            pieceRadius = self.track.trackPieces[self.myCar.pieceIndex].radiusses[self.myCar.startlane]
        
        if pieceRadius == 0:
            self.track.friction = 0
        else:
            self.track.friction = self.myCar.speed*self.myCar.speed/pieceRadius
        
        #Store parameters for logging
        if current_piece.curve == True:
            self.loggingArray.append((self.myCar.speed,self.myCar.angle,current_piece.radiusses[self.myCar.startlane],current_piece.angle))
            
        if self.myCar.lap == 0:
            self.throttle(0.65)
        if self.myCar.lap == 1:
            self.throttle(0.63)
        if self.myCar.lap == 2:
            self.throttle(0.61)

    def on_crash(self, data):
        print("Someone crashed")
        self.ping()
        
    def on_spawn(self, data):
        print("Someone returned on track")
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.ping()
        
    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()
    
    def on_your_car(self, data):
        print("Your Car")
        self.myCar.name = data["name"]
        self.myCar.color = data["color"]
    
    def on_game_init(self, data):
        print("Game init")
        trackObj = data['race']['track']
        self.track = Track(trackObj)
        if 'durationMs' in data['race']['raceSession']:
            self.quickRace = False
        # TODO: Better logic for quick race vs. qualifying vs. race.
        
    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'spawn': self.on_spawn,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'gameInit': self.on_game_init,
            'yourCar': self.on_your_car,
            'turboAvailable' : self.on_turbo,
        }
        
        # log_file = open('log.txt', 'w')
        socket_file = self.socket.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            if 'gameTick' in msg:
                self.tick = msg['gameTick']
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()
        # log_file.close()

