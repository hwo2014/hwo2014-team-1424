import json
import socket
import math
import csv
from track import Track
from Car import Car
from noobBot import NoobBot

STATE_WAITING_FOR_START = 0
STATE_TESTING_CRASH = 1
STATE_TESTING_FRICTION = 2
STATE_RACING = 3


class PekkaBot4(NoobBot):

    def __init__(self, socket, name, key):
        NoobBot.__init__(self, socket, name, key)
        self.state = STATE_WAITING_FOR_START
        self.lastSwitchCommand = ""
        self.maximumAngleForCrash = -1
        self.startAccTestTick = -1
        self.xFactor = -1
        
    def on_car_positions(self, data):
        # print json.dumps(data, sort_keys=True, indent=4, separators=(',', ': '))
        #update data for myCar
        tickDelta = self.tick - self.lastTick
        self.lastTick = self.tick
        
        for car in data:
            if car["id"]["name"] == self.myCar.name:
                oldAngle = self.myCar.angle
                self.myCar.updateFromJson(car)
                newDistanceFromStart = self.track.distanceFromStart(self.myCar.pieceIndex, self.myCar.inPieceDistance, self.myCar.startlane, self.myCar.lap)
                self.myCar.calculateSpeed(newDistanceFromStart, tickDelta)
                self.myCar.calculateAngleSpeed(oldAngle, tickDelta)
                break
        
        self.currentPiece = self.track.trackPieces[self.myCar.pieceIndex]
        pedal = 0.625
        
        """
        command = self.track.commandForNextSwitch(self.myCar)
        if command != self.lastSwitchCommand:
            self.lastSwitchCommand = command
            print "send switch"
            self.switch(command)
            return
         """   
        #start testing acceleration
        if self.state == STATE_WAITING_FOR_START:
            print "testing acc"
            self.state = STATE_TESTING_CRASH
        elif self.state == STATE_TESTING_CRASH:
            #when we come to curve or reach speed 9, calculate car acceleration            
            if self.myCar.carAcceleration == 0:
                if self.currentPiece.curve == True or self.myCar.speed >= 9:
                    self.myCar.carAcceleration = self.myCar.speed/(self.tick-self.startAccTestTick) 
                    print "Found car acceleration",self.myCar.carAcceleration
            
            #Dont accelerate in curves
            #if self.currentPiece.curve:
                #if self.myCar.angle == 0:
                #    pedal = 1.0#self.myCar.speed/10
                #else:
                #    pedal = self.myCar.speed/10
            #Full pedal until crash
        elif self.state == STATE_TESTING_FRICTION:
            if self.currentPiece.index == self.track.indexOfLongestStraight and self.myCar.turbo == True:
                print "TURBO"
                self.turbo("TURBO")
                return;
                
            nextcurve, distance = self.track.nextCurve(self.myCar.pieceIndex, self.myCar.inPieceDistance, self.myCar.endlane)
            targetSpeedForNextCurve = math.sqrt(self.xFactor*nextcurve.radiusses[self.myCar.endlane])
            
            if self.currentPiece.curve == False:
                targetSpeedForThisCurve = 999
            else:
                targetSpeedForThisCurve = math.sqrt(self.xFactor*nextcurve.radiusses[self.myCar.endlane])
                
            vi = self.myCar.speed
            dv = self.myCar.speed-targetSpeedForNextCurve
            a = self.myCar.carAcceleration
            deccelerationDistance = vi*dv/a - 0.5*dv*dv/a
            
            if distance < abs(deccelerationDistance):
                if self.myCar.speed > targetSpeedForNextCurve:
                    pedal = 0
                else:
                    pedal = targetSpeedForNextCurve/10
            else:
                #pedal = 1.0
                pedal = targetSpeedForThisCurve/10
            
            """
            command = self.track.commandForNextSwitch(self.myCar)
            if command != self.lastSwitchCommand:
                self.lastSwitchCommand = command
                print "send switch"
                self.switch(command)
                return
            
            testingEnded = False     
            #check for piece changes during first lap
            if self.currentCurvePieceIndex == self.currentPiece.curveGroupIndex and self.currentPiece.curveGroupIndex != -1:
                if abs(self.myCar.angle) > self.maxAngleForCurrentCurveGroup:
                    self.maxAngleForCurrentCurveGroup = abs(self.myCar.angle)
            else:
                if self.currentCurvePieceIndex != -1:
                    self.curveGroupDatas.append(self.maxAngleForCurrentCurveGroup)
                    if len(self.curveGroupDatas) > self.track.curveGroupCount:
                        testingEnded = True
                self.currentCurvePieceIndex = self.currentPiece.curveGroupIndex
                self.maxAngleForCurrentCurveGroup = 0

            
            if testingEnded == True:
                print self.curveGroupDatas
                
                #find maximum angle from curveGroups
                maxIndex = 0
                maxAngle = 0
                
                for i in range(0,len(self.curveGroupDatas)):
                    curveAngle = self.curveGroupDatas[i]
                    if curveAngle > maxAngle:
                        maxIndex = i
                        maxAngle = curveAngle
                        
                #compute xFactor = (RACE_MAX_CURVE_SPEED-START_TEST_SPEED)/sqrt(angleForMaxGroup)
                #maxAngle = 1.1*maxAngle
                    
                self.xFactor = (RACE_MAX_CURVE_SPEED-START_TEST_SPEED)/math.sqrt(maxAngle)
                #compute speeds for curveGroups
                #targetSpeed = RACE_MAX_CURVE_SPEED-xFactor*sqrt(angleForGroup)
                for i in range(0,len(self.curveGroupDatas)):
                    angle = self.curveGroupDatas[i]
                    self.curveGroupSpeeds[i] = RACE_MAX_CURVE_SPEED-self.xFactor*math.sqrt(angle)
                
                
                print "speeds",self.curveGroupSpeeds
          
                self.state = STATE_RACING
                print "racing"
        elif self.state == STATE_RACING:
            if self.currentPiece.index == self.track.indexOfLongestStraight and self.myCar.turbo == True:
                print "TURBO LAUNCHED"
                self.turbo("TURBO")
                return;
            
            command = self.track.commandForNextSwitch(self.myCar)
            if command != self.lastSwitchCommand:
                self.lastSwitchCommand = command
                print "send switch"
                self.switch(command)
                return
            
            nextcurve, distance = self.track.nextCurve(self.myCar.pieceIndex, self.myCar.inPieceDistance, self.myCar.endlane)
            targetSpeedForNextCurve = self.curveGroupSpeeds[nextcurve.curveGroupIndex]
            
            if self.currentPiece.curve == False:
                targetSpeedForThisCurve = 999
            else:
                targetSpeedForThisCurve = self.curveGroupSpeeds[self.currentPiece.curveGroupIndex]
            
            
            vi = self.myCar.speed
            dv = self.myCar.speed-targetSpeedForNextCurve
            a = self.myCar.carAcceleration
            deccelerationDistance = vi*dv/a - 0.5*dv*dv/a
            
            if distance < abs(deccelerationDistance):
                if self.myCar.speed > targetSpeedForNextCurve:
                    pedal = 0
                else:
                    pedal = targetSpeedForNextCurve/10
            else:
                #pedal = 1.0
                pedal = targetSpeedForThisCurve/10
                if pedal > 1.0:
                    pedal = 1.0
            """
            #print "targetSpeedForThisCurve",targetSpeedForThisCurve,"targetSpeedForNextCurve",targetSpeedForNextCurve,"speed",self.myCar.speed,"pedal",pedal
        else:
            print"Unknown state"
        self.loggingArray.append((self.lastTick,self.myCar.speed,self.myCar.angle,self.myCar.angleSpeed,self.myCar.angleAcceleration,self.currentPiece.radiusses[self.myCar.endlane],self.currentPiece.angle))
        #print("index",self.currentPiece.index,"speed",self.myCar.speed,"angle",self.myCar.angle,"angleSpeed",self.myCar.angleSpeed,"angleAcc",self.myCar.angleAcceleration,"radius",self.currentPiece.radiusses[self.myCar.endlane],"pieceAngle",self.currentPiece.angle)
        
        v= self.myCar.speed
        r =self.currentPiece.radiusses[self.myCar.endlane]
        carAngle = self.myCar.angle
        angleAcc = self.myCar.angleAcceleration
        
        #if carAngle != 0:
        #    print("k",(v*v/r-angleAcc)/carAngle)
        #Store parameters for logging
#        self.loggingArray.append((self.myCar.angle,self.myCar.speed,currentPiece.radiusses[self.myCar.startlane],currentPiece.angle))
        if self.startAccTestTick == -1:
            self.startAccTestTick = self.tick
            
        if pedal > 1.0:
            pedal = 1.0  
        if pedal < 0.0:
            pedal = 0.0
                      
        self.throttle(pedal)

    def on_crash(self, data):
        print("Someone crashed")
        print "Piece index",self.currentPiece.index,"radius",self.currentPiece.radiusses[self.myCar.endlane],"angle",self.currentPiece.angle,"speed",self.myCar.speed
        if self.state == STATE_TESTING_CRASH:
            self.maximumAngleForCrash = abs(self.myCar.angle)
            #self.state = STATE_TESTING_FRICTION
            if self.currentPiece.curve == False:
                crashCurve,dist = self.track.previousCurve(self.currentPiece.index)
            else:
                crashCurve = self.currentPiece
            
            v = 0.8*self.myCar.speed
            self.xFactor = v*v/crashCurve.radiusses[self.myCar.startlane]
            #Write parameters from race to csv file
            #with open('log.csv', 'wb') as f:
            #    writer = csv.writer(f)
            #    writer.writerows(bot.loggingArray)
            print "found max crash angle",self.maximumAngleForCrash,"xFactor",self.xFactor
        elif self.state == STATE_TESTING_FRICTION:
            self.xFactor = 0.9*self.xFactor
            print "crashed new xFactor",self.xFactor


        self.ping()
            
