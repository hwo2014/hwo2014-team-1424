import math


class Car(object):
    
    def __init__(self,name,color):
        self.speed = 0
        self.startlane = 0
        self.endlane = 0
        self.lap = 0
        self.name = name
        self.color = color
        self.pieceIndex = 0  
        self.inPieceDistance = 0
        self.acceleration = 0#current acceleration based on current speed change
        self.carAcceleration = 0#car acceleration calculated during start acceleration. Speed change when throttle is 1.0
        self.carDecceleration = 0
        self.angle = 0
        self.angleSpeed = 0
        self.angleAcceleration = 0
        self.turbo = False
        
        self.previousPieceIndex = 0
        self.previousInPieceDistance = 0
        self.coveredDistance = 0
        self.switchedInPreviousPiece = False

    def updateFromJson(self, data): 
        self.angle = data["angle"]
        self.lap = data["piecePosition"]["lap"]
        self.pieceIndex = data["piecePosition"]["pieceIndex"]
        self.inPieceDistance = data["piecePosition"]["inPieceDistance"]
        self.startlane = data["piecePosition"]["lane"]["startLaneIndex"]
        self.endlane = data["piecePosition"]["lane"]["endLaneIndex"]
        
    def calculateSpeed(self, timeInterval, track, switching):
        if timeInterval > 0:
            
            oldDistance = self.coveredDistance

            if self.previousPieceIndex == self.pieceIndex:
                self.coveredDistance += self.inPieceDistance - self.previousInPieceDistance
            else:
                previousPiece = track.piece(self.previousPieceIndex)
                if self.switchedInPreviousPiece:
                    self.coveredDistance += previousPiece.length(self.endlane) * 1.0205 - self.previousInPieceDistance
                else:
                    self.coveredDistance += previousPiece.length(self.endlane) - self.previousInPieceDistance
                self.coveredDistance += self.inPieceDistance
                
            self.switchedInPreviousPiece = switching
            
            newSpeed = (self.coveredDistance - oldDistance) / timeInterval
            self.acceleration = (newSpeed-self.speed)/timeInterval
            self.speed = newSpeed
            
            self.previousPieceIndex = self.pieceIndex
            self.previousInPieceDistance = self.inPieceDistance
            
    def calculateAngleSpeed(self, oldAngle, timeInterval):
        if timeInterval > 0:
            oldAngleSpeed = self.angleSpeed
            self.angleSpeed = (self.angle-oldAngle)/timeInterval
            self.angleAcceleration = (self.angleSpeed-oldAngleSpeed/timeInterval)
