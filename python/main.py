import csv #for logging
import json
import logging
import socket
import sys

from noobBot import NoobBot
from terobot import TeroBot
from pekkaBot import PekkaBot
from pekkaBot2 import PekkaBot2
from pekkaBot3 import PekkaBot3
from pekkaBot4 import PekkaBot4


if __name__ == "__main__":
    if len(sys.argv) != 9 and len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
        print("Usage: ./run host port botname botkey create trackName trackPassword trackCarCount")
    else:
        host, port, name, key = sys.argv[1:5]
        
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        
        if len(sys.argv) == 9:
            create, trackName, trackPassword, trackCarCount = sys.argv[5:9]
            print("create={0}, trackName={1}, pass={2}, cars={3}".format(*sys.argv[5:9]))
        else:
            create = "join"
            trackName = ""
            trackPassword = ""
            trackCarCount = 0
            
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        
        if name == 'noob':
            bot = NoobBot(s, name, key)
        elif name == 'iceman_pekka':
            bot = PekkaBot(s,name,key)
        elif name == 'iceman_pekka2':
            bot = PekkaBot2(s,name,key)
        elif name == 'iceman_pekka3':
            bot = PekkaBot3(s,name,key)
        elif name == 'iceman_pekka4':
            bot = PekkaBot4(s,name,key)            
        else:
            # For testing CI run.
            bot = TeroBot(s, name, key)
        
        try:
            bot.run(create,trackName,trackPassword,trackCarCount)
        except:
            logging.exception('crash')

        #Write parameters from race to csv file
        with open('log.csv', 'wb') as f:
            writer = csv.writer(f)
            writer.writerows(bot.loggingArray)

