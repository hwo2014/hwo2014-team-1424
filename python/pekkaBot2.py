import json
import socket
import math
from track import Track
from Car import Car
from noobBot import NoobBot

STATE_WAITING_FOR_START = 0
STATE_TESTING_ACCELERATION = 1
STATE_TESTING_FRICTION = 2
STATE_RACING = 3
START_TEST_SPEED = 6.25


class PekkaBot2(NoobBot):

    def __init__(self, socket, name, key):
        NoobBot.__init__(self, socket, name, key)
        self.state = STATE_WAITING_FOR_START
        self.currentCurvePieceIndex = -1
        self.angleChanges = []
        self.maxAngleCoeff = 0
        self.angles = []
        self.maxAngleBeforeCrash = 0
        self.lastSwitchCommand = ""
        
    def on_car_positions(self, data):
        # print json.dumps(data, sort_keys=True, indent=4, separators=(',', ': '))
        #update data for myCar
        tickDelta = self.tick - self.lastTick
        self.lastTick = self.tick
        
        for car in data:
            if car["id"]["name"] == self.myCar.name:
                oldAngle = self.myCar.angle
                self.myCar.updateFromJson(car)
                newDistanceFromStart = self.track.distanceFromStart(self.myCar.pieceIndex, self.myCar.inPieceDistance, self.myCar.startlane, self.myCar.lap)
                self.myCar.calculateSpeed(newDistanceFromStart, tickDelta)
                break
        
        currentPiece = self.track.trackPieces[self.myCar.pieceIndex]
        pedal = START_TEST_SPEED/10
        
        #start testing acceleration
        if self.state == STATE_WAITING_FOR_START:
            print "testing acc"
            self.state = STATE_TESTING_ACCELERATION
            pedal = 1.0
        elif self.state == STATE_TESTING_ACCELERATION:
            pedal = 1.0
            #when test speed is achieved, calculate car acceleration and start testing friction
            if self.myCar.speed >= START_TEST_SPEED:
                self.myCar.carAcceleration = self.myCar.speed/self.tick
                self.state = STATE_TESTING_FRICTION
                print "achieved test speed, acceleration",self.myCar.carAcceleration
        elif self.state == STATE_TESTING_FRICTION: 
            
            command = self.track.commandForNextSwitch(self.myCar)
            if command != self.lastSwitchCommand:
                self.lastSwitchCommand = command
                print "send switch"
                self.switch(command)
                return
                                       
            #check for piece changes
            if currentPiece.curve == True and self.currentCurvePieceIndex != currentPiece.index:
                #calculate only after first curve
                if self.currentCurvePieceIndex != -1:
                    prevPiece = self.track.trackPieces[self.currentCurvePieceIndex]
                    angleChange = self.curveStartAngle-self.myCar.angle
                    self.angleChanges.append((angleChange/prevPiece.length(self.myCar.startlane),prevPiece.radiusses[self.myCar.startlane],self.myCar.angle,self.myCar.speed))
                    
                    centrifugal = prevPiece.radiusses[self.myCar.startlane]*angleChange/(prevPiece.angle*self.myCar.speed*self.myCar.speed)
                    print "changed curve, enter angle",self.curveStartAngle,"exit angle", self.myCar.angle,"at speed",self.myCar.speed
                    print "piece radius",prevPiece.radiusses[self.myCar.endlane],"angle",prevPiece.angle,"centrifugal",centrifugal    
                    #self.loggingArray.append((self.curveStartAngle,self.myCar.angle,angleChange,prevPiece.radiusses[self.myCar.startlane],prevPiece.angle,angleChange/prevPiece.length(self.myCar.startlane)))
                    
                self.currentCurvePieceIndex = self.myCar.pieceIndex
                self.curveStartAngle = self.myCar.angle
                self.angles.append(self.myCar.angle)
            
            if self.myCar.lap == 1:
                minValue = 9999
                minIndex = 0
                for i in range(0,len(self.angleChanges)):
                    changeTuple = self.angleChanges[i]
                    change = changeTuple[0]
                    if abs(change) < minValue:
                        minValue = abs(change)
                        minIndex = i
                
                minAngleChangeTuple = self.angleChanges[minIndex]
                r = minAngleChangeTuple[1]
                maxA = minAngleChangeTuple[2]
                v = minAngleChangeTuple[3]
                print "maxA",maxA,"r",r,"v",v,"minIndex",minIndex
                #self.maxAngleCoeff = abs(v*v/(maxA*r))
                self.maxAngleCoeff = abs(maxA*r/(v*v))
                print self.maxAngleCoeff
                
                maxAngleValue = 0
                for i in range(0,len(self.angles)):
                    thisAngle = abs(self.angles[i])
                    if (thisAngle > maxAngleValue):
                        maxAngleValue = thisAngle
                        
                self.maxAngleBeforeCrash = maxAngleValue 
                print "maxAngleBeforeCrash",self.maxAngleBeforeCrash               
                self.state = STATE_RACING
                print "racing"
        elif self.state == STATE_RACING:
            if currentPiece.index == self.track.indexOfLongestStraight and self.myCar.turbo == True:
                print "TURBO"
                self.turbo("TURBO")
                return;
            #Try to estimate max angle
            
            nextcurve, distance = self.track.nextCurve(self.myCar.pieceIndex, self.myCar.inPieceDistance, self.myCar.endlane)
            r=nextcurve.radiusses[self.myCar.endlane]
            rthis = currentPiece.radiusses[self.myCar.endlane]
            targetSpeedForNextCurve = 0.8*math.sqrt(r*self.maxAngleBeforeCrash/self.maxAngleCoeff)
            if rthis == 0:
                targetSpeedForThisCurve = 999
            else:
                targetSpeedForThisCurve = math.sqrt(self.maxAngleCoeff*rthis*self.maxAngleBeforeCrash)
            
            print "targetSpeedForNextCurve",targetSpeedForNextCurve,"mySpeed",self.myCar.speed
            
            if self.myCar.speed < targetSpeedForNextCurve and self.myCar.speed < targetSpeedForThisCurve:
                print "full pedal"
                pedal = 1.0
            else:
                index = self.myCar.pieceIndex
                vi = self.myCar.speed
                dv = self.myCar.speed-targetSpeedForNextCurve
                a = self.myCar.carAcceleration
                deccelerationDistance = vi*dv/a - 0.5*dv*dv/a
                print "acc",self.myCar.carAcceleration,"targetSpeed",targetSpeedForNextCurve,"mySpeed",self.myCar.speed
                print "deccelerationDistance",deccelerationDistance

                if nextcurve.index < index:
                    nextCurveDistanceFromStart = self.track.distanceFromStart(index, 0, self.myCar.startlane, self.myCar.lap-1)
                else:
                    nextCurveDistanceFromStart = self.track.distanceFromStart(index, 0, self.myCar.startlane, self.myCar.lap)
                    
                if (self.myCar.distanceFromStart + deccelerationDistance > nextCurveDistanceFromStart):
                    print"slow down"
                    pedal = 0.0
                else:
                    print "full pedal 2"
                    pedal = 1.0
            """
            #This is just for logging
            if currentPiece.curve == True and self.currentCurvePieceIndex != currentPiece.index:
                r=currentPiece.radiusses[self.myCar.startlane]
                v=self.myCar.speed
                estimatedMaxAngle = v*v/(r*self.maxAngleCoeff)
                if currentPiece.angle < 0:
                    estimatedMaxAngle = -estimatedMaxAngle
                print "carAngle",self.myCar.angle
                print "estimatedMaxAngle",estimatedMaxAngle
                self.currentCurvePieceIndex = self.myCar.pieceIndex
            
            if self.myCar.lap == 1:
                pedal = 0.6
            else:
                pedal = 0.55
            """
            #change pedal
        else:
            print"Unknown state"
        
        #Store parameters for logging
        self.loggingArray.append((self.myCar.angle,self.myCar.speed,currentPiece.radiusses[self.myCar.startlane],currentPiece.angle))

        self.throttle(pedal)
        
        
        
        """
        else 
        tickDelta = self.tick - self.lastTick
        self.lastTick = self.tick

        current_piece = self.track.piece(self.myCar.piece)

        #Calculate friction. When car starts to slide at an angle, friction should be v*v/r
        pieceRadius = 0
        if self.track.trackPieces[self.myCar.piece].curve:
            pieceRadius = self.track.trackPieces[self.myCar.piece].radiusses[self.myCar.startlane]
        
        if pieceRadius == 0:
            self.track.friction = 0
        else:
            self.track.friction = self.myCar.speed*self.myCar.speed/pieceRadius
        """
        

